<?php

include __DIR__.'/../vendor/firebase/php-jwt/src/BeforeValidException.php';
include __DIR__.'/../vendor/firebase/php-jwt/src/ExpiredException.php';
include __DIR__.'/../vendor/firebase/php-jwt/src/SignatureInvalidException.php';
include __DIR__.'/../vendor/firebase/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;

function validate($jwt) {
  include 'config.php';
  if($jwt) {
    try {
      $decoded = JWT::decode($jwt, $key, array('HS256'));
      return $decoded->data->id;
    }
    catch (Exception $e) {  
      return false;
    }
  } else {
    return false;
  }
}

?>
