<?php

include __DIR__.'/../vendor/firebase/php-jwt/src/BeforeValidException.php';
include __DIR__.'/../vendor/firebase/php-jwt/src/ExpiredException.php';
include __DIR__.'/../vendor/firebase/php-jwt/src/SignatureInvalidException.php';
include __DIR__.'/../vendor/firebase/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;

function generate($user) {
  include 'config.php';
  try {
    $token = array(
      "iat" => $issued_at,
      "exp" => $expiration_time,
      "iss" => $issuer,
      "data" => array(
          "id" => $user->id,
          "firstname" => $user->firstname,
          "lastname" => $user->lastname,
          "email" => $user->email
      )
    );
    return array(
      "success" => true,
      "JWT" => JWT::encode($token, $key),
    );
  } catch (Exception $e) {
    return array(
      "success" => false,
      "JWT" => null,
    );
  }
}

?>