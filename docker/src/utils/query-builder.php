<?php

function dot($k) {
  return ':'.$k;
}

function build_update_query($keys, $table, $where_key) {
  $keys_comma = implode(', ', $keys);
  $keys_dotted = implode(', ', array_map("dot", $keys));

  return "UPDATE {$table} SET ({$keys_comma}) = ({$keys_dotted}) WHERE {$where_key} = :{$where_key}";
}

?>