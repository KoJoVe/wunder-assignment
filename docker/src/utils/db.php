<?php

include 'constants.php';

$db = new SQLite3('wunder.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

$db->enableExceptions(true);
$db->exec("PRAGMA foreign_keys = ON;");

$db->exec(
  "CREATE TABLE IF NOT EXISTS {$GLOBALS["USER_TABLE"]} (
    id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, 
    email TEXT UNIQUE NOT NULL, 
    pass TEXT NOT NULL,
    firstname TEXT, 
    lastname TEXT, 
    telephone TEXT, 
    iban TEXT, 
    account_owner TEXT,
    payment TEXT 
  );"
);

$db->exec(
  "CREATE TABLE IF NOT EXISTS {$GLOBALS["ADDRESS_TABLE"]} (
    user INTEGER REFERENCES {$GLOBALS["USER_TABLE"]} UNIQUE NOT NULL, 
    street TEXT NOT NULL,
    num INTEGER,
    city TEXT NOT NULL,
    comp TEXT,
    zip TEXT NOT NULL
  );"
);

?>