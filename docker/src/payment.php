<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
class PaymentRequest {
  public function __construct($method) {
    if($method == 'GET') {
      $this->get();
    }
  }

  private function authenticated() {
    include 'utils/validation.php';
    
    $result = null;
    foreach (getallheaders() as $name => $value) { 
      if($name == 'Authorization') {
        $result = validate($value);
      }
    }

    if(!$result) {
      http_response_code(401);
      $res = new StdClass();
      $res->message = "Unauthorized";
      echo json_encode($res);  
      return false; 
    }

    return $result;
  }

  private function get() {
    include 'utils/db.php';
    include 'models/user-model.php';

    $id = $this->authenticated();
    if(!$id) {
      return;
    }

    $data = new StdClass();
    $data->id = $id;
    $user = new User($db);
    $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    try {
      if(!$user->get($data)) {
        throw new Exception('User Get Failed');
      }

      if(!isset($user->id) || !isset($user->iban) || !isset($user->account_owner)) {
        throw new Exception('User Missing Data (IBAN or Account Owner)'); 
      }

      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
          "customerId": '.$user->id.',
          "iban": "'.$user->iban.'",
          "owner": "'.$user->account_owner.'"
        }',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
        ),
      ));
      
      $response = curl_exec($curl);
      $data->payment = json_decode($response)->paymentDataId;
      curl_close($curl);

      if(!$response) {
        throw new Exception('Payment Post Failed'); 
      }

      if(!$user->update($data)) {
        throw new Exception('Payment id Save Failed'); 
      }
      
      http_response_code(200);
      $res = new StdClass();
      $res->message = "Data Posted Successfully";
      $res->data = new StdClass();
      $res->data->payment = $data->payment;
      echo json_encode($res);
    } catch (Exception $e) {
      http_response_code(400);
      $res = new StdClass();
      $res->message = "Request Invalid";
      echo json_encode($res);  
    }
  }
}

$method = $_SERVER['REQUEST_METHOD'];
$request = new PaymentRequest($method);

?>