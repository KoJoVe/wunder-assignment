<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Expose-Headers: Authorization");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
class LoginRequest {
  public function __construct($method) {
    if($method == 'POST') {
      $this->post($_POST);
    }
  }

  private function post() {
    include_once('utils/db.php');
    include_once('utils/token.php');
    include_once('models/user-model.php');

    $data = json_decode(file_get_contents("php://input"));

    $user = new User($db);
    if($user->validate($data)) {
      try {
        $jwt = generate($user)['JWT'];
        header("Authorization: {$jwt}");
        http_response_code(200);
        $res = new StdClass();
        $res->message =  "User Logged In Successfuly";
        echo json_encode($res);
      } catch(Exception $e) {
        http_response_code(500);
        $res = new StdClass();
        $res->message = "JWT Generation Falied";
        echo json_encode($res);
      }
      return;
    }
    http_response_code(401);
    $res = new StdClass();
    $res->message = "Login Falied";
    echo json_encode($res);
  }
}

$method = $_SERVER['REQUEST_METHOD'];
$request = new LoginRequest($method);

?>