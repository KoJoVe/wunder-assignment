<?php

include_once(__DIR__ .'/../utils/constants.php');

class Address {

  private $conn;

  public $user;
  public $street;
  public $num;
  public $comp;
  public $zip;
  public $city;

  public function __construct($db){
    $this->conn = $db;
  }

  function get($data) {
    try {
      $stmt = $this->conn->prepare("SELECT * FROM {$GLOBALS["ADDRESS_TABLE"]} WHERE user = :user LIMIT 0,1");
      $stmt->bindValue(':user', htmlspecialchars(strip_tags($data->id)), SQLITE3_INTEGER);
      $result = $stmt->execute();
  
      if(!($row = $result->fetchArray())) {
        return false;
      }

      $this->user = $row['user'];
      $this->street = $row['street'];
      $this->num = $row['num'];
      $this->comp = $row['comp'];
      $this->zip = $row['zip'];
      $this->city = $row['city'];

      return true;
    } catch (Exception $e) {
      return false;
    }
  }

  function create($data) {
    try {
      $stmt=$this->conn->prepare("INSERT INTO {$GLOBALS["ADDRESS_TABLE"]} (user, street, city, zip) VALUES (:user, '', '', '')");
      $stmt->bindValue(":user", htmlspecialchars(strip_tags($data->id)), SQLITE3_INTEGER);
      if($stmt->execute()) {
        return true;    
      } else {
        throw new Exception('Statement Failed'); 
      };  
    } catch (Exception $e) {
      return false;
    }
  }

  function update($data) {
    include_once(__DIR__ .'/../utils/query-builder.php');

    $error = false;

    $keys = array();
    foreach($data as $key => $val) {
      if($key == 'id' || !isset($val)) {
        continue;
      }
      array_push($keys, $key);
    }

    try {
      $stmt=$this->conn->prepare(build_update_query($keys, $GLOBALS["ADDRESS_TABLE"], 'user'));
      $stmt->bindValue(":user", htmlspecialchars(strip_tags($data->id)), SQLITE3_INTEGER);
      
      foreach($keys as $key) {
        $type = gettype($data->$key) == 'string' ? SQLITE3_TEXT : SQLITE3_INTEGER;
        $value = htmlspecialchars(strip_tags($data->$key));
        $stmt->bindValue(
          ":{$key}", 
          $value, 
          $type,
        );
      }
      
      $stmt->execute();
      return true;
    } catch (Exception $e) {
      return false;
    }
  }
}

?>