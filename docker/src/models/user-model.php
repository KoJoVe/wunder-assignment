<?php

include_once(__DIR__ .'/../utils/constants.php');

class User {

  private $conn;

  public $id;
  public $email;
  public $firstname;
  public $lastname;
  public $telephone;
  public $iban;
  public $account_owner;
  public $payment;
  public $address;

  public function __construct($db) {
    $this->conn = $db;
  }

  function validate($data) {

    if(!property_exists($data, 'email') || !property_exists($data, 'pass')) {
      return false;
    }

    try {
      $stmt = $this->conn->prepare("SELECT * FROM {$GLOBALS["USER_TABLE"]} WHERE email = :email LIMIT 0,1");
      $stmt->bindValue(':email', htmlspecialchars(strip_tags($data->email)), SQLITE3_TEXT);
      $result = $stmt->execute();
  
      if(!($row = $result->fetchArray())) {
        return false;
      }

      if(password_verify(htmlspecialchars(strip_tags($data->pass)), $row['pass'])) {
        $this->id = $row['id'];
        $this->email = $row['email'];
        $this->firstname = $row['firstname'];
        $this->lastname = $row['lastname'];
        return true;
      }

      return false; 
    } catch (Exception $e) {
      return false;
    }
  }

  function get($data) {
    include_once('address-model.php');

    try {
      $stmt = $this->conn->prepare("SELECT * FROM {$GLOBALS["USER_TABLE"]} WHERE id = :id LIMIT 0,1");
      $stmt->bindValue(':id', htmlspecialchars(strip_tags($data->id)), SQLITE3_INTEGER);
      $result = $stmt->execute();
  
      if(!($row = $result->fetchArray())) {
        return false;
      }

      $this->id = $row['id'];
      $this->email = $row['email'];
      $this->firstname = $row['firstname'];
      $this->lastname = $row['lastname'];
      $this->telephone = $row['telephone'];
      $this->iban = $row['iban'];
      $this->account_owner = $row['account_owner'];
      $this->payment = $row['payment'];

      $this->address = new Address($this->conn);

      return $this->address->get($data);
    } catch (Exception $e) {
      return false;
    }
  }

  function create($data) {
    include_once('address-model.php');

    if(!property_exists($data, 'email') || !property_exists($data, 'pass')) {
      return false;
    }

    try {
      $stmt=$this->conn->prepare("INSERT INTO {$GLOBALS["USER_TABLE"]} (email, pass) VALUES (:email, :pass)");
      $stmt->bindValue(":email", htmlspecialchars(strip_tags($data->email)), SQLITE3_TEXT);
      $stmt->bindValue(":pass", password_hash(htmlspecialchars(strip_tags($data->pass)), PASSWORD_BCRYPT), SQLITE3_TEXT);
      $stmt->execute();
      
      $stmt = $this->conn->prepare("SELECT * FROM {$GLOBALS["USER_TABLE"]} WHERE email = :email LIMIT 0,1");
      $stmt->bindValue(':email', htmlspecialchars(strip_tags($data->email)), SQLITE3_TEXT);
      $result = $stmt->execute();
  
      if(!($row = $result->fetchArray())) {
        return false;
      }

      $this->address = new Address($this->conn);
      $address_data = new StdClass();
      $address_data->user = $row['id'];
      
      if($result && $this->address->create($address_data)) {
        return true;    
      } else {
        throw new Exception('Statement Failed'); 
      };  
    } catch (Exception $e) {
      return false;
    }
  }

  function update($data) {
    include_once('address-model.php');
    include_once(__DIR__ .'/../utils/query-builder.php');

    $error = false;

    $keys = array();
    foreach($data as $key => $val) {
      if($key == 'id' || $key == 'address' || !isset($val)) {
        continue;
      }
      array_push($keys, $key);
    }

    try {
      if(count($keys) > 0) {
        $stmt=$this->conn->prepare(build_update_query($keys, $GLOBALS["USER_TABLE"], 'id'));
        $stmt->bindValue(":id", htmlspecialchars(strip_tags($data->id)), SQLITE3_INTEGER);  
      }

      foreach($keys as $key) {
        $type = SQLITE3_TEXT;
        $value = null;
        
        if($key == 'pass') {
          $value = password_hash(htmlspecialchars(strip_tags($data->$key)), PASSWORD_BCRYPT);
        } else {
          $type = gettype($data->$key) == 'string' ? SQLITE3_TEXT : SQLITE3_INTEGER;
          $value = htmlspecialchars(strip_tags($data->$key));
        }

        $stmt->bindValue(
          ":{$key}", 
          $value, 
          $type,
        );
      }
      
      if(count($keys) > 0) {
        $stmt->execute();
      }

      if(property_exists($data, 'address')) {
        $this->address = new Address($this->conn);
        $address_data = $data->address;
        $address_data->id = $data->id;    
        if(!$this->address->update($address_data)) {
          return false;
        }
      }

      return true;
    } catch (Exception $e) {
      return false;
    }
  }
}

?>