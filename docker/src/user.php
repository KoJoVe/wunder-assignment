<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST, PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
class UserRequest {
  public function __construct($method) {
    if($method == 'GET') {
      $this->get();
    } else if($method == 'POST') {
      $this->post();
    } else if($method == 'PUT') {
      $this->put();
    }
  }

  private function authenticated() {
    include_once('utils/validation.php');
    
    $result = null;
    foreach (getallheaders() as $name => $value) { 
      if($name == 'Authorization') {
        $result = validate($value);
      }
    }

    if(!$result) {
      http_response_code(401);
      $res = new StdClass();
      $res->message = "Unauthorized";
      echo json_encode($res);  
      return false; 
    }

    return $result;
  }

  private function get() {
    include_once('utils/db.php');
    include_once('models/user-model.php');

    $id = $this->authenticated();
    if(!$id) {
      return;
    }

    $data = new StdClass();
    $data->id = $id;

    $user = new User($db);

    if($user->get($data)) {
      http_response_code(200);
      $res = new StdClass();
      $res->message = "User Retrieved Successfully";
      $res->data = $user;
      echo json_encode($res);
      return;
    }

    http_response_code(400);
    $res = new StdClass();
    $res->message = "Request Invalid";
    echo json_encode($res);
  }

  private function post() {
    include_once('utils/db.php');
    include_once('models/user-model.php');

    $data = json_decode(file_get_contents("php://input"));
    $user = new User($db);

    if($user->create($data)) {
      http_response_code(200);
      $res = new StdClass();
      $res->message = "User Created Successfully";
      echo json_encode($res);
      return;
    }
    
    http_response_code(400);
    $res = new StdClass();
    $res->message = "Request Invalid";
    echo json_encode($res);
  }

  private function put() {
    include_once('utils/db.php');
    include_once('models/user-model.php');

    $id = $this->authenticated();
    if(!$id) {
      return;
    }

    $data = json_decode(file_get_contents("php://input"));
    $data->id = $id;

    $db->exec('BEGIN');
    $user = new User($db);

    if($user->update($data)) {
      $db->exec('COMMIT');
      http_response_code(200);
      $res = new StdClass();
      $res->message = "User Updated Successfully";
      echo json_encode($res);
      return;
    }

    $db->exec('ROLLBACK');
    http_response_code(400);
    $res = new StdClass();
    $res->message = "Request Invalid";
    echo json_encode($res);
  }
}

$method = $_SERVER['REQUEST_METHOD'];
$request = new UserRequest($method);

?>