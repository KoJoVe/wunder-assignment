import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getUser, updateUser, getPayment } from '../../service';

const StyledContainer = styled(Container)`
  margin-top: 100px;
  text-align: center;
  color: #555;
`;

const StyledH4 = styled.h4`
  margin-top: 5px !important;
  word-wrap: break-word;
`;

const StyledH6 = styled.h6`
  margin-top: 25px !important;
`;

const StyledTextField = styled(TextField)`
  margin-top: 25px !important;
`;

const StyledCircularProgress = styled(CircularProgress)`
  margin-top: 25px !important;
`;

const StyledButton = styled(Button)`
  margin-top: 25px !important;
  margin-left: 10px !important;
  margin-right: 10px !important;
`;

export const Payment = ({ onFinish, onBack }) => {
  const [data, setData] = useState({
    account_owner: '',
    iban: '',
    payment: ''
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const getUserInfo = async () => {
    setLoading(true);
    const response = await getUser();
    setData({
      account_owner: response.account_owner,
      iban: response.iban,
      payment: response.payment,
    });
    setLoading(false);
  };

  useEffect(() => {
    getUserInfo();
  }, []);

  const handlePrimaryButton = async () => {
    setError(false);
    setLoading(true);
    const response = await updateUser({
      account_owner: data.account_owner,
      iban: data.iban,      
    });
    if(response) {
      const payment = await getPayment();
      if(payment) {
        setData({ ...data, payment });
        onFinish && onFinish();
      }
    } else {
      setError(true);
    }
    setLoading(false);
  }

  const handleSecondaryButton = async () => {
    onBack && onBack();
  }

  return (
    <StyledContainer maxWidth="sm">
      
      <h1>Payment Info</h1>
      <h4>Please enter your payment info. Your payment ID will be informed right after.</h4>
      
      <StyledTextField 
        label="Account Owner" 
        variant="filled" 
        value={data.account_owner}
        disabled={loading}
        onChange={(e) => 
          setData({...data, account_owner: e.target.value})
        } />
      <br />
      
      <StyledTextField 
        label="IBAN" 
        variant="filled" 
        value={data.iban}
        disabled={loading}
        onChange={(e) => 
          setData({...data, iban: e.target.value})
        } />
      <br />
      
      { loading ? <StyledCircularProgress /> : null }
      { loading ? <br /> : null }

      { error ? <StyledH6>An error has occured!</StyledH6> : null }
      { error ? <br /> : null }

      { data.payment ? <StyledH6>Successfully set up payment ID:</StyledH6> : null }
      { data.payment ? <StyledH4>{data.payment}</StyledH4> : null }
      { data.payment ? <br /> : null }
      
      <StyledButton 
        color="primary"
        variant="outlined"
        disabled={loading}
        onClick={handleSecondaryButton} >
        Back
      </StyledButton>
      
      <StyledButton 
        color="primary"
        variant="contained"
        onClick={handlePrimaryButton}
        disabled={loading || !data.account_owner || !data.iban} >
        Finish
      </StyledButton>

    </StyledContainer>
  );
}

export default Payment;
