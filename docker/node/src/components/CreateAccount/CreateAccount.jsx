import React, { useState } from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createUser } from '../../service';

const StyledContainer = styled(Container)`
  margin-top: 100px;
  text-align: center;
  color: #555;
`;

const StyledH6 = styled.h6`
  margin-top: 25px !important;
`;

const StyledTextField = styled(TextField)`
  margin-top: 25px !important;
`;

const StyledCircularProgress = styled(CircularProgress)`
  margin-top: 25px !important;
`;

const StyledButton = styled(Button)`
  margin-top: 25px !important;
  margin-left: 10px !important;
  margin-right: 10px !important;
`;

export const CreateAccount = ({ onBack }) => {
  const [data, setData] = useState({
    email: '',
    pass: '',
    cpass: ''
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const handlePrimaryButton = async () => {
    setError(false);
    setLoading(true);
    const response = await createUser({
      email: data.email,
      pass: data.pass
    });
    if(response) {
      setSuccess(true);
    } else {
      setError(true);
    }
    setLoading(false);
  }

  const handleSecondaryButton = async () => {
    onBack && onBack();
  }

  return (
    <StyledContainer maxWidth="sm">
      
      <h1>Create Account</h1>
      <h4>Please enter your email, password and password confirmation</h4>
      
      <StyledTextField 
        label="Email" 
        variant="filled" 
        value={data.email}
        disabled={success || loading}
        onChange={(e) => 
          setData({...data, email: e.target.value})
        } />
      <br />
      
      <StyledTextField 
        type="password"
        label="Password" 
        variant="filled" 
        value={data.pass}
        disabled={success || loading}
        onChange={(e) => 
          setData({...data, pass: e.target.value})
        } />
      <br />

      <StyledTextField 
        type="password"
        label="Confirm Password" 
        variant="filled" 
        value={data.cpass}
        disabled={success || loading}
        onChange={(e) => 
          setData({...data, cpass: e.target.value})
        } />
      <br />
      
      { loading ? <StyledCircularProgress /> : null }
      { loading ? <br /> : null }

      { error ? <StyledH6>An error has occured!</StyledH6> : null }
      { error ? <br /> : null }

      { success ? <StyledH6>Account created successfully! Please return to previous screen and Sign In</StyledH6> : null }
      { success ? <br /> : null }
      
      <StyledButton 
        color="primary"
        variant="outlined"
        disabled={loading}
        onClick={handleSecondaryButton} >
        Back
      </StyledButton>
      
      <StyledButton 
        color="primary"
        variant="contained"
        onClick={handlePrimaryButton}
        disabled={success || loading || !data.email || !data.pass || !(data.pass === data.cpass)} >
        Sign In
      </StyledButton>

    </StyledContainer>
  );
}

export default CreateAccount;
