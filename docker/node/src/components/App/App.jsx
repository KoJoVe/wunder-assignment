import React, { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import Login from '../Login/Login';
import CreateAccount from '../CreateAccount/CreateAccount';
import Personal from '../Personal/Personal';
import Address from '../Address/Address';
import Payment from '../Payment/Payment';
import { getAuthorization, setAuthorization, getUser } from '../../service';

export const App = () => {
  const [cookies, setCookie] = useCookies(['wunder_assignment_auth', 'wunder_assignment_step']);
  const [state, setState] = useState({
    step: 0,
  });

  const setStep = (step) => {
    setState({ ...state, step });

    if(step !== 0 && step !== -1) {
      setCookie('wunder_assignment_auth', getAuthorization());
      setCookie('wunder_assignment_step', step);
    }
  }

  const verifyAuthorization = async () => {
    if(cookies.wunder_assignment_auth != null && cookies.wunder_assignment_step != null) {
      setAuthorization(cookies.wunder_assignment_auth);
      const response = getUser();
      if(response) {
        setState({ ...state, step: parseInt(cookies.wunder_assignment_step)});
      } else {
        setAuthorization('');
      }
    }
  }

  useEffect(() => {
    verifyAuthorization();
    // eslint-disable-next-line
  }, []);

  const setFinishedCookies = () => {
    setCookie('wunder_assignment_auth', getAuthorization());
    setCookie('wunder_assignment_step', state.step);
  }

  return (
    <div>
      { state.step === -1 ? <CreateAccount onBack={() => setStep(0) } /> : null }
      { state.step === 0 ? <Login onLogin={() => setStep(1)} onSignUp={() => setStep(-1) } /> : null }
      { state.step === 1 ? <Personal onNext={() => setStep(2)} onBack={() => setStep(0) } /> : null }
      { state.step === 2 ? <Address onNext={() => setStep(3) } onBack={() => setStep(1) } /> : null }
      { state.step === 3? <Payment onFinish={() => setFinishedCookies()} onBack={() => setStep(2) } /> : null }
    </div>
  );
}

export default App;
