import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getUser, updateUser } from '../../service';

const StyledContainer = styled(Container)`
  margin-top: 100px;
  text-align: center;
  color: #555;
`;

const StyledH6 = styled.h6`
  margin-top: 25px !important;
`;

const StyledTextField = styled(TextField)`
  margin-top: 25px !important;
`;

const StyledCircularProgress = styled(CircularProgress)`
  margin-top: 25px !important;
`;

const StyledButton = styled(Button)`
  margin-top: 25px !important;
  margin-left: 10px !important;
  margin-right: 10px !important;
`;

export const Address = ({ onNext, onBack }) => {
  const [data, setData] = useState({
    street: '',
    num: null,
    zip: '',
    comp: '',
    city: '',  
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const getUserInfo = async () => {
    setLoading(true);
    const response = await getUser();
    setData({
      street: response.address && response.address.street,
      num: response.address && response.address.num,
      zip: response.address && response.address.zip,
      comp: response.address && response.address.comp,
      city: response.address && response.address.city,
    });
    setLoading(false);
  };

  useEffect(() => {
    getUserInfo();
  }, []);

  const handlePrimaryButton = async () => {
    setError(false);
    setLoading(true);

    const newData = {
      address: {
        street: data.street,
        num: data.num,
        zip: data.zip,
        comp: data.comp,
        city: data.city
      }
    };

    if(!newData.address.num) {
      delete newData.address.num;
    }

    if(!newData.address.comp) {
      delete newData.address.comp;
    }

    const response = await updateUser(newData);
    if(response) {
      onNext && onNext();
    } else {
      setError(true);
    }
    setLoading(false);
  }

  const handleSecondaryButton = async () => {
    onBack && onBack();
  }

  return (
    <StyledContainer maxWidth="sm">
      
      <h1>Address Info</h1>
      <h4>Please enter your address. Number and complement are optional.</h4>
      
      <StyledTextField 
        label="Street" 
        variant="filled" 
        value={data.street}
        disabled={loading}
        onChange={(e) => 
          setData({...data, street: e.target.value })
        } />
      <br />
      
      <StyledTextField
        type="number"
        label="Number" 
        variant="filled" 
        value={data.num}
        disabled={loading}
        onChange={(e) => 
          setData({...data, num: e.target.value })
        } />
      <br />

      <StyledTextField 
        label="Complement" 
        variant="filled" 
        value={data.comp}
        disabled={loading}
        onChange={(e) => 
          setData({...data, comp: e.target.value })
        } />
      <br />

      <StyledTextField 
        label="City" 
        variant="filled" 
        value={data.city}
        disabled={loading}
        onChange={(e) => 
          setData({...data, city: e.target.value })
        } />
      <br />

      <StyledTextField 
        label="Zip Code" 
        variant="filled" 
        value={data.zip}
        disabled={loading}
        onChange={(e) => 
          setData({...data, zip: e.target.value })
        } />
      <br />
      
      { loading ? <StyledCircularProgress /> : null }
      { loading ? <br /> : null }

      { error ? <StyledH6>An error has occured!</StyledH6> : null }
      { error ? <br /> : null }
      
      <StyledButton 
        color="primary"
        variant="outlined"
        disabled={loading}
        onClick={handleSecondaryButton} >
        Back
      </StyledButton>
      
      <StyledButton 
        color="primary"
        variant="contained"
        onClick={handlePrimaryButton}
        disabled={loading || !data.street || !data.city || !data.zip} >
        Next Step
      </StyledButton>

    </StyledContainer>
  );
}

export default Address;
