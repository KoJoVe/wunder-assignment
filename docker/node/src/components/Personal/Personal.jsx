import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getUser, updateUser } from '../../service';

const StyledContainer = styled(Container)`
  margin-top: 100px;
  text-align: center;
  color: #555;
`;

const StyledH6 = styled.h6`
  margin-top: 25px !important;
`;

const StyledTextField = styled(TextField)`
  margin-top: 25px !important;
`;

const StyledCircularProgress = styled(CircularProgress)`
  margin-top: 25px !important;
`;

const StyledButton = styled(Button)`
  margin-top: 25px !important;
  margin-left: 10px !important;
  margin-right: 10px !important;
`;

export const Personal = ({ onNext, onBack }) => {
  const [data, setData] = useState({
    firstname: '',
    lastname: '',
    telephone: ''
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  
  const getUserInfo = async () => {
    setLoading(true);
    const response = await getUser();
    setData({
      firstname: response.firstname,
      lastname: response.lastname,
      telephone: response.telephone,
    });
    setLoading(false);
  };

  useEffect(() => {
    getUserInfo();
  }, []);

  const handlePrimaryButton = async () => {
    setError(false);
    setLoading(true);
    const response = await updateUser(data);
    if(response) {
      onNext && onNext();
    } else {
      setError(true);
    }
    setLoading(false);
  }

  const handleSecondaryButton = async () => {
    onBack && onBack();
  }

  return (
    <StyledContainer maxWidth="sm">
      
      <h1>Personal Info</h1>
      <h4>Please enter your first name, last name and phone number.</h4>
      
      <StyledTextField 
        label="First Name" 
        variant="filled" 
        value={data.firstname}
        disabled={loading}
        onChange={(e) => 
          setData({...data, firstname: e.target.value})
        } />
      <br />
      
      <StyledTextField 
        label="Last Name" 
        variant="filled" 
        value={data.lastname}
        disabled={loading}
        onChange={(e) => 
          setData({...data, lastname: e.target.value})
        } />
      <br />

      <StyledTextField 
        label="Telephone" 
        variant="filled" 
        value={data.telephone}
        disabled={loading}
        onChange={(e) => 
          setData({...data, telephone: e.target.value})
        } />
      <br />
      
      { loading ? <StyledCircularProgress /> : null }
      { loading ? <br /> : null }

      { error ? <StyledH6>An error has occured!</StyledH6> : null }
      { error ? <br /> : null }
      
      <StyledButton 
        color="primary"
        variant="outlined"
        disabled={loading}
        onClick={handleSecondaryButton} >
        Back
      </StyledButton>
      
      <StyledButton 
        color="primary"
        variant="contained"
        onClick={handlePrimaryButton}
        disabled={loading || !data.firstname || !data.lastname || !data.telephone} >
        Next Step
      </StyledButton>

    </StyledContainer>
  );
}

export default Personal;
