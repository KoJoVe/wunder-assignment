import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { login, setAuthorization, getUser } from '../../service';

const StyledContainer = styled(Container)`
  margin-top: 100px;
  text-align: center;
  color: #555;
`;

const StyledH6 = styled.h6`
  margin-top: 25px !important;
`;

const StyledTextField = styled(TextField)`
  margin-top: 25px !important;
`;

const StyledCircularProgress = styled(CircularProgress)`
  margin-top: 25px !important;
`;

const StyledButton = styled(Button)`
  margin-top: 25px !important;
  margin-left: 10px !important;
  margin-right: 10px !important;
`;

export const Login = ({ onLogin, onSignUp }) => {
  const [data, setData] = useState({
    email: '',
    pass: ''
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const getUserInfo = async () => {
    setLoading(true);
    const response = await getUser();
    if(response) {
      setData({
        email: response.email,
        pass: ''
      });  
    }
    setLoading(false);
  };

  useEffect(() => {
    getUserInfo();
  }, []);

  const handlePrimaryButton = async () => {
    setError(false);
    setLoading(true);
    const response = await login(data);
    if(response) {
      setAuthorization(response);
      onLogin && onLogin();
    } else {
      setError(true);
    }
    setLoading(false);
  }

  const handleSecondaryButton = async () => {
    onSignUp && onSignUp();
  }

  return (
    <StyledContainer maxWidth="sm">
      
      <h1>Welcome</h1>
      <h4>Please login or create an account.</h4>
      
      <StyledTextField 
        label="Email" 
        variant="filled" 
        value={data.email}
        disabled={loading}
        onChange={(e) => 
          setData({...data, email: e.target.value})
        } />
      <br />
      
      <StyledTextField 
        type="password"
        label="Password" 
        variant="filled" 
        value={data.pass}
        disabled={loading}
        onChange={(e) => 
          setData({...data, pass: e.target.value})
        } />
      <br />
      
      { loading ? <StyledCircularProgress /> : null }
      { loading ? <br /> : null }

      { error ? <StyledH6>An error has occured!</StyledH6> : null }
      { error ? <br /> : null }
      
      <StyledButton 
        color="primary"
        variant="outlined"
        disabled={loading}
        onClick={handleSecondaryButton} >
        Sign Up
      </StyledButton>
      
      <StyledButton 
        color="primary"
        variant="contained"
        onClick={handlePrimaryButton}
        disabled={loading || !data.email || !data.pass} >
        Sign In
      </StyledButton>

    </StyledContainer>
  );
}

export default Login;
