import axios from 'axios';

export const getAuthorization = () => {
  return axios.defaults.headers.common['Authorization'];
}

export const setAuthorization = (token) => {
  axios.defaults.headers.common['Authorization'] = token;
}

export const getPayment = async () => {
  const config = {
    method: 'get',
    url: 'http://localhost:8080/payment.php',
    headers: { 
      'Content-Type': 'application/json'
    }
  };
  try {
    return (await axios(config)).data.data.payment;
  } catch (e) {
    return false;
  }
}

export const getUser = async () => {
  const config = {
    method: 'get',
    url: 'http://localhost:8080/user.php',
    headers: { 
      'Content-Type': 'application/json'
    }
  };
  try {
    return (await axios(config)).data.data;
  } catch (e) {
    return false;
  }
}

export const createUser = async (data) => {
  const config = {
    method: 'post',
    url: 'http://localhost:8080/user.php',
    headers: { 
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(data)
  };
  try {
    return (await axios(config)).data;
  } catch (e) {
    return false;
  }
}

export const updateUser = async (data) => {
  const config = {
    method: 'put',
    url: 'http://localhost:8080/user.php',
    headers: { 
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(data)
  };
  try {
    return (await axios(config)).data;
  } catch (e) {
    return false;
  }
}

export const login = async (data) => {
  const config = {
    method: 'post',
    url: 'http://localhost:8080/login.php',
    headers: { 
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(data)
  };
  try {
    return (await axios(config)).headers.authorization;
  } catch (e) {
    return false;
  }
}

// eslint-disable-next-line
export default { getAuthorization, setAuthorization, getPayment, getUser, createUser, updateUser, login };