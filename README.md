# Wunder Application Project
This project consists of a small user data CRUD, using a PHP server, SQLite database and a React SPA. 

The React app has 5 distinct screens:

 - **Create Account**
 -  **Login**
 - **Personal Info**
 - **Address Info**
 - **Payment Info**

Each screen has a different amount of fields that should be filled by the user and saved in the database. The user should be able to return to the process of filling the fields from where he stopped if he closes the website. At the end of the forms, a paymentId should be retreived from a Wunder API, shown to the user and stored on the database.

# How to Run
To run the project locally, simply download and install **docker-compose:**
- https://docs.docker.com/compose/install/

Then navigate to the project **docker** folder and run the following:

    docker-compose build

This will generate all of the images needed to run the project. Then run:

    docker-compose up

After that, docker should install all dependencies needed and start the development servers.

# Usage
Access http://localhost:3000/ on your favorite browser, and enjoy! :)

# Final Observations
- Since the app uses a SQLite3 database, the export of the data is easily stored on the **wunder.sqlite** file.

- Since the core of the assignment is a simple CRUD, major performance improvements were not needed. The most important performance oriented code bit is located on **user-model.php** starting on **line 122** which saves all keys to be updated on the user and then create a query using all the keys, erasing the need to use multiple **UPDATE** commands on a unique request.

- What I think could be done better: Use a more robust framework for the back-end, something like **Laravel** would spare a great deal of work. Also, all of the screens on the front-end share a very similar structure. This could lead to the creation of a component that could be extended and configured according to the user needs, without the necessity to rewrite large portions of code
